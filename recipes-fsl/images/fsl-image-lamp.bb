require recipes-fsl/images/fsl-image-machine-test.bb

DESCRIPTION = "Extend fsl-image-machine-test providing a LAMP Server"

CORE_IMAGE_EXTRA_INSTALL += "lighttpd lighttpd-module-fastcgi sqlite3 php-cli php-cgi"

export IMAGE_BASENAME = "fsl-image-lamp"


