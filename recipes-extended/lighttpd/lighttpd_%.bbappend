FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://info.php file://lighttpd.conf"

do_install_append() {
  install -d  ${D}/etc
  install -m 0644 ${WORKDIR}/lighttpd.conf ${D}/etc
  
  install -d ${D}/www/pages
  install -m 0644 ${WORKDIR}/info.php ${D}/www/pages
}
